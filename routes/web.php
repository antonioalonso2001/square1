<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductsController@index');
Route::post('/','ProductsController@post');

Route::get('/wishlists/add/{product}',['as'=>'wishlist.add','uses'=>'WishlistsController@add'])->middleware('auth');
Route::get('/wishlists/delete/{wishlists}',['as'=>'wishlist.delete','uses'=>'WishlistsController@delete'])->middleware('auth');
Route::get('/wishlists/{wishlist}',['as'=>'wishlists','uses'=>'WishlistsController@index']);

Auth::routes();
