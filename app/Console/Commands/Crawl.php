<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Products;
use Goutte;

class Crawl extends Command
{
    private $_products = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:appliances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl the appliances website to fetch data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $urls = config('crawl.urls');
        
        $error = false;
 
        foreach($urls as $url)
        {
            try{
                echo $url;
                
                $crawler = Goutte::request('GET',$url);

                //get the last page number
                $last_page_url = explode('=',$crawler->filter('.result-list-pagination a:last-child')->attr('href'));
                $last_page = intval(array_pop($last_page_url)); 

                $page = 1;
                
                echo " Done\n";
                
                do{                 
                    $crawler->filter('.search-results-product')->each(function ($node) {
                        //get the product id from the product URL
                        $product_url = explode('/',$node->filter('.product-image a')->attr('href'));
                        $product_id = array_pop($product_url);                         
                         $this->_products [$product_id] = [
                            'id' =>  $product_id,
                            'image' => $node->filter('.product-image img')->attr('src'),
                            'name' => $node->filter('.product-description h4 a')->text(),
                            'price' => floatval(str_replace(['€',','],'',$node->filter('.product-description h3')->text()))
                        ];

                    });
 
                    $new_url = $url.(strpos($url,'?')!==false?'&':'?').'page='.(++$page);
                    echo $new_url;
                    
                    $crawler = Goutte::request('GET',$new_url);  
                    echo " Done\n";
                    
                } while($page <= $last_page);

            } catch (\Exception $ex) {
                $error = true;
            }
        }            

        if(sizeof($this->_products) > 0 && !$error)
        {
            //clear the table                        
            Products::truncate();
            Products::insert($this->_products);                    
        }
        else{
            echo "\nUnable to retrieve the catalog. Try again later.\n";
        }
        
        echo "End\n";
        
        return true;
    }
}
