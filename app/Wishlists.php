<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlists extends Model
{
   protected $guarded = [];
   
      
   public function products()
   {
       return $this->belongsTo('App\Products');
   }
}
