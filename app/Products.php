<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Products extends Model
{
   protected $guarded = [];
   
   public function wishlists()
   {
       return $this->hasMany(Wishlists::class);
   }
   
   public function mine()
   {
       $user_id = null;
       if(Auth::user())
       {
          $user_id = Auth::user()->id; 
       }
        return $this->hasMany('App\Wishlists')->whereUserId( $user_id);
   }

   public function addToWishlist($user_id)
   {
       if(!$this->wishlists()->where('user_id',$user_id)->first())
       {
           $this->wishlists()->create(['user_id'=>$user_id]);
       }
       
   }
}
