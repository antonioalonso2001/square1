<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Wishlists;
use Illuminate\Support\Facades\Auth;

class WishlistsController extends Controller
{
    public function index($user_id){
        $products = Wishlists::where('user_id',$user_id)->has('products')->paginate(24);
        $logged_user_id = null;
        
       if(Auth::user())
       {
          $logged_user_id = Auth::user()->id; 
       }
        
        return view('wishlist',[
            'products'=>$products,
            'user_id'=>$logged_user_id,
            'list_id' => $user_id
        ]
        );
        
    }
    
    public function add(Products $product) {
        
        if($product)
        {
            $user_id = Auth::user()->id;
            $product->addToWishlist($user_id);
            return redirect()->back()->with('message','<strong>'.$product->name.'</strong> has been added to <a href="'.route('wishlists',[$user_id]).'">your Wishlist</a>');
  
        }
        
        return redirect()->back();
    }
    
    public function delete(Wishlists $wishlists){
        $user_id = Auth::user()->id;
        
        if($wishlists && $wishlists->user_id == $user_id)
        {
            $product = $wishlists->products;
            $wishlists->delete();
            return redirect()->back()->with('message','<strong>'.$product->name.'</strong> has been removed from your Wishlist');
            
        }   
        
        return redirect()->back();
    }
}
