<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;


class ProductsController extends Controller
{
    public function index(Products $products) {
        
        $order = session('orderBy','name');
        $products = $products->orderBy($order)->with('mine')->paginate(24);
                
        return view('list',[
            'products'=>$products,
            'order' => $order]);
    }
    
    public function post(Request $request)             
    {
       
        session(['orderBy'=>$request->input('sort')]);
        
        return redirect()->back();
    }
}
