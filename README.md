# square1

The app uses Laravel 5.6 so the hosting needs the requirements listed in https://laravel.com/docs/5.6#server-requirements
Composer needs to be installed in the server too.

Notes

The catalog gets updated every hour thanks to a artisan command "crawl:appliances" that fetches the data fomr the URLs.
-The frecuency of the updates can be changed in App/Console/Kernel.php

Installation.

- Clone the repository "git clone https://bitbucket.org/antonioalonso2001/square1.git"
- Enter in the newly created square1 directory and install it with composer "composer install"
- Rename .env.example to .env and edit the file and to modify the DDBB configuration to fit your needs.
- Run "php artisan migrate" to generate de database structure.
- Run "php artisan crawl:appliances" lo fetch the catalog for the first time
- Add the following cron task to your system: * * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1 

You are all set.

Enjoy!