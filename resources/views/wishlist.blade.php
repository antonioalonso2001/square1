@extends('layouts.app')

@section('content')

<div class="container">
    @if(count($products)>0)
    <p class="lead text-center">Share it: <a href="{{ route('wishlists',[$list_id]) }}">{{ route('wishlists',[$list_id]) }}</a></p>
    @else
    <p class="lead text-center">The list is empty! <a href="{{ url('/')}}">Add Some Products</a></p>
    @endif
    <div class="row">    
    @foreach($products as $product)

    <div class="col-md-4">
      <div class="card mb-4 box-shadow text-center">
        <div class="card-header">
            <h5 class="card-title">{{$product->products->name}}</h5>
        </div>
        <img class="card-img-top" src="{{$product->products->image}}" alt="Card image cap">
        <div class="card-body">
          <div class="text-right">
            @auth
              @if ($user_id == Auth::user()->id)
              <a href="{{ route('wishlist.delete',[$product->id]) }}"  class="btn btn-md btn-danger">Delete</a>
              @endif
           @endauth              
          </div>
        </div>
        <div class="card-footer text-muted">
        <p class="card-text">€{{ number_format($product->products->price,2)}}</p>
   
      </div>
    </div>
    </div>
    
    @endforeach          
        
    </div>
</div>

@endsection