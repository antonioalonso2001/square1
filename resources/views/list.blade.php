@extends('layouts.app')

@section('content')
    
@include('layouts.toolbar')
 
<div class="container">
    <div class="row">    
    
    @foreach($products as $product)

    <div class="col-md-4">
      <div class="card mb-4 box-shadow text-center">
        <div class="card-header">
            <h5 class="card-title">{{$product->name}}</h5>
        </div>
        <img class="card-img-top" src="{{$product->image}}" alt="Card image cap">
        <div class="card-body">
          <div class="text-right">
              @if(sizeof($product->mine) == 0)
              <a href="{{ route('wishlist.add',[$product->id]) }}"  class="btn btn-md btn-primary">Add to Whishlist</a>
              @else
              <span class="btn btn-md btn-success">Added</span>
              @endif
          </div>
        </div>
        <div class="card-footer text-muted">
    <p class="card-text">€{{ number_format($product->price,2)}}</p>

  </div>
      </div>
    </div>
    
    @endforeach     
    
    </div>
            
    {{ $products->links()}}

</div>
@endsection