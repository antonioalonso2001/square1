<div class="container"><nav class="navbar navbar-expand-lg navbar-light bg-light">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sort By        
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item {{ $order == 'name'?'active':''}}" href="#" onclick="event.preventDefault();document.getElementById('sort-input').value = 'name';
                                                     document.getElementById('sort-form').submit();">Title</a>
          <a class="dropdown-item {{ $order == 'price'?'active':''}}" href="#" onclick="event.preventDefault();document.getElementById('sort-input').value = 'price';
                                                     document.getElementById('sort-form').submit();">Price</a>
        </div>
      </li>
    </ul>
<form id="sort-form" action="" method="POST" style="display: none;">
    <input name="sort" id="sort-input" value="" type="hidden">
    @csrf
</form>        
</nav>
</div>